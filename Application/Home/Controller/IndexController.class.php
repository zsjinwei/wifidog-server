<?php
namespace Home\Controller;
use Think\Controller;
class IndexController extends Controller {
	private $is_mobile = false;
	private $valid_agent = true;
	private $defaultUrl = "http://www.baidu.com";
	private $is_online = false;
    /**
     * 构造函数
     */
	function __construct()
	{
		parent::__construct();
		//获取user_agent将来对客户端进行限定		
		//$temp_str = $this->input->user_agent() ;

		//if(!(!empty($temp_str) and $temp_str == 'WiFiDog 20131017'))
		//	$this->valid_agent = false;
        
		//根据 http user_agent 判断访问者的设备类型，主要用在login，及portal接口上
		$this->is_mobile = isMobile();			
	}

	public function dashboard(){
		//if(session('gw_id')!='' && session('gw_port')!='' && session('gw_address')!='' && session('uid')!='' && session('username')!=''){
			//判断session参数，若有参数设置，则跳转到控制台
			session('start_online_time', 0);
			$this->username = session('username');
			$this->last_login_time = date('Y-m-d H:i:s', session('logintime'));
			$last_online_data = M('user_last_online_data')->where(array('username'=>session('username')))->find();
			$this->last_online_time = secFormat($last_online_data['last_online_time']);
			$this->last_online_flow = flowFormat($last_online_data['last_online_flow']);
			$this->start_online_time = session('start_online_time');
			$this->is_online = $this->is_online;
			$this->remainflow = flowFormat(session('remainflow'));
			$this->display();
		//}
		//else{
		    //否则跳转到默认页面
		//	echo "<script>";  
    	//	echo "window.location.href = '". $this->defaultUrl . "';";  
    	//	echo "</script>"; 
		//}
	}

	public function logout(){
		session_unset();
        session_destroy();
        echo "<script>";  
    	echo "window.location.href = '". $this->defaultUrl . "';";  
    	echo "</script>"; 
	}

	public function onlineDataQuery(){
		if(!IS_AJAX) E('页面不存在！');
		if(session('username')!='' && session('uid')!=''){
			$this->ajaxReturn(array(
				'status'=>'1', 
				'msg'=>'请求成功。', 
				'remainflow'=>session('remainflow'),
				'is_online' => ($this->is_online))
			, 'json');
		}
		else{
			E('页面不存在！');
		}
	}

	/**
	 * 上线处理
	 * @return [type] [description]
	 */
	public function on_line(){
		if(session('gw_id')!='' && session('gw_port')!='' && session('gw_address')!='' && session('uid')!='' && session('username')!='' && session('username')!=''){
			$this->is_online = true;
			$redirectUrl = 'http://'.$_SESSION['gw_address'].':'.$_SESSION['gw_port'].'/wifidog/auth?token='.$_SESSION['token'].'&url='.$_SESSION['url'];
			session('start_online_time', time());
			session('start_online_flow', session('remainflow'));
		}
		else{
			$redirectUrl = $this->defaultUrl;
		}
		echo "<script>";  
    	echo "window.location.href = '". $redirectUrl . "';";  
    	echo "</script>"; 
	}

	/**
	 * 下线处理
	 * @return [type] [description]
	 */
	public function off_line(){
		if(session('gw_id')!='' && session('gw_port')!='' && session('gw_address')!='' && session('uid')!='' && session('username')!='' && session('username')!=''){
			$this->is_online = false;
			$redirectUrl = U('/Home/Index/dashboard');
			$online_time = time() - session('start_online_time');
			$online_flow = session('start_online_flow') - session('remainflow');
			M('user_last_online_time')->where(array('username'=>session('username')))->delete();
			M('user_last_online_time')->add(array('username'=>session('username'), 'last_online_time'=>$online_time, 'last_online_flow'=>$online_flow));
		}
		else{
			$redirectUrl = $this->defaultUrl;
		}
		echo "<script>";  
    	echo "window.location.href = '". $redirectUrl . "';";  
    	echo "</script>"; 
	}

	/**
	 * 默认页面
	 * @return [type] [description]
	 */
    public function index(){
    	//显示空白，或者显示给普通访问者的页面
		echo "hello,wifidog!";
    }

    /**
     * ping心跳连接处理接口，wifidog会按照配置文件的间隔时间，定期访问这个接口，以确保认证服务器“健在”！
     */
	public function ping()
	{
		if(!$this->valid_agent)
			return;
		//url请求 "gw_id=$gw_id&sys_uptime=$sys_uptime&sys_memfree=$sys_memfree&sys_load=$sys_load&wifidog_uptime=$wifidog_uptime";
		//
		//判断各种参数是否为空
		if( !(isset($_GET['gw_id']) and isset($_GET['sys_uptime']) and isset($_GET['sys_memfree']) and isset($_GET['sys_load']) and isset($_GET['wifidog_uptime']) ) )
		{
			echo '{"error":"2"}';
			return;
		}
		//添加心跳日志处理功能
		/*
		此处可获取 wififog提供的 如下参数
		1.gw_id  来自wifidog 配置文件中，用来区分不同的路由设备
		2.sys_uptime 路由器的系统启动时间
		3.sys_memfree 系统内存使用百分比
		4.wifidog_uptime wifidog持续运行时间（这个数据经常会有问题）		
		*/
		
		//返回值
		echo 'Pong';
	}

	/**
     * 认证用户登录页面
	 * 该页面用来用各种方式（用户名名、密码，随机码，微博，微信，qq，手机号码等）判定使用者的身份！
	 * 
	 * 认证后要做的事情：	1.认证不通过，还是继续回到该页面（大不要丢掉刚开始wifidog传递上来的参数）
	 *						2.通过认证：根据wifidog的参数，做页面重定向						
	 *
     */
	public function login()
	{	
		/*
		wifidog 带过来的参数主要有
		1. gw_id        网关ID（默认设置为网关的MAC地址）
		2. gw_address   wifidog状态的访问地址
		3. gw_port 	    wifidog状态的访问端口
		4. url 	        被重定向的url（用户访问的url）
		5. mac          客户端MAC地址
		请求示例：gw_address=192.168.8.1&gw_port=2060&gw_id=B068B6FF6846&mac=68:17:29:29:19:22&url=http%3A//www.google.com/
		*/	
		//记录wifidog传来的参数到session，以便其他方法获取
		session('gw_id', I('gw_id', ''));
		session('url', I('url', ''));
		session('gw_port', I('gw_port', ''));
		session('gw_address', I('gw_address', ''));
		session('mac', I('mac', ''));
		//if(session('gw_id')!='' && session('gw_port')!='' && session('gw_address')!=''){
		    // 判断请求来自wifidog网关还是直接访问
		    recordLog("网关ID:".session('gw_id').",设备:".session('mac').",通过 ".session('gw_address').":".session('gw_port')." 请求认证。", time());
			$this->display();
		//}
		//else{
		    //直接访问是跳转到默认页面
		//	echo "<script>";  
    	//	echo "window.location.href = '". $this->defaultUrl . "';";  
    	//	echo "</script>"; 
		//}
	}	

	/**
	 * 登录表单处理函数
	 * @return [type] [description]
	 */
	public function loginFormHandle(){
		if(!IS_AJAX) E('页面不存在！');
    	$username = I('username', '');
      	$password = I('password', '', 'md5');
      	if($username!='' && $password!=''){
      		$user = M('user')->where(array('username' => $username, 'password' => $password))->find();
      		if($user){
      			if($user['islock']!='1'){
	      			//写入session
	        		session('uid', $user['id']);
	        		session('username', $user['username']);
	        		session('logintime', $user['logintime']);
	        		session('email', $user['email']);
	        		session('token', md5(uniqid(rand(), 1)));
	        		session('remainflow', $user['remainflow']);
	        		//$redirectUrl = 'http://'.$_SESSION['gw_address'].':'.$_SESSION['gw_port'].'/wifidog/auth?token='.md5(uniqid(rand(), 1)).'&url='.$_SESSION['url'];
					M('user')->where(array('id' => $user['id']))->save(array('logintime' => time()));
					recordLog($user['username'].'登录成功！', time());
					$this->ajaxReturn(array('status' => '1', 'Msg' => '登录成功！'), 'json');
				}
				else{
					recordLog($user['username'].'登录失败！', time());
					$this->ajaxReturn(array('status' => '0', 'Msg' => '此用户已经锁定，请联系管理员！'), 'json');
				}
      		}
      		else{
      			recordLog($user['username'].'登录失败！', time());
      			$this->ajaxReturn(array('status' => '0', 'Msg' => '用户名或者密码错误！'), 'json');
      		}
      		
      	}
      	else{
      		$this->ajaxReturn(array('status' => '0', 'Msg' => '用户名或者密码不能为空！'), 'json');
      	}
	}

	/**
	 * 注册方法
	 * @return [type] [description]
	 */
	public function signup()
	{
		$this->redirectUrl = $this->defaultUrl;
		$this->display();	
	}

	/**
     * 认证接口
     */
	public function auth()
	{
		if(!$this->valid_agent && session('token')=='')
			return;
		//响应客户端的定时认证，可在此处做各种统计、计费等等
		/*
		wifidog 会通过这个接口传递连接客户端的信息，然后根据返回，对客户端做开通、断开等处理，具体返回值可以看wifidog的文档
		wifidog主要提交如下参数
		1. ip
		2. mac
		3. token（login页面下发的token）
		4. incoming 下载流量
		5. outgoing 上传流量 
		6. stage    认证阶段，就两种 login 和 counters
		*/
		$stage = $_GET['stage'] == 'counters'?'counters':'login';
		if($stage == 'login')
        {
			//WiFiDog再次确认授权
			$token = session('token');
			if($token == I('token', '')){
				echo "Auth: 1";
			}
			else{
				echo "Auth: 0";
			}
			
		}
		else if($stage == 'counters')
		{
			$remainflow = session('remainflow');
			session('remainflow', $remainflow - I('incoming', 0)); //更新剩余流量
			if(session('remainflow') < 0){
				session('remainflow', 0);
			}
			M('user')->where(array('username'=>session('username')))->save(array('remainflow'=>session('remainflow')));
			if($this->is_online){
				if(session('remainflow')>0){
					echo "Auth: 1";
				}
				else{
					echo "Auth: 0";
				}
			}
			else{
				echo "Auth: 0";
			}
		}
		else{
			echo "Auth: 0"; //其他情况都返回拒绝
		}
			
			
		/*
		返回值：主要有这两种就够了
		0 - 拒绝
		1 - 放行
		
		官方文档如下
		0 - AUTH_DENIED - User firewall users are deleted and the user removed.
		6 - AUTH_VALIDATION_FAILED - User email validation timeout has occured and user/firewall is deleted（用户邮件验证超时，防火墙关闭该用户）
		1 - AUTH_ALLOWED - User was valid, add firewall rules if not present
		5 - AUTH_VALIDATION - Permit user access to email to get validation email under default rules （用户邮件验证时，向用户开放email）
		-1 - AUTH_ERROR - An error occurred during the validation process
		*/
	}

	/**
     * portal 跳转接口
     */
	public function portal()
	{
		/*
			wifidog 带过来的参数 如下
			1. gw_id
		*/
		
		//重定到指定网站 或者 显示splash广告页面		
		redirect('http://www.baidu.com', 'location', 302);
			
	}

    /**
     * wifidog 的gw_message 接口，信息提示页面
     */
    function gw_message()
    {
        if (isset($_REQUEST["message"])) {
            switch ($_REQUEST["message"]) {
                case 'failed_validation': 
				//auth的stage为login时，被服务器返回AUTH_VALIDATION_FAILED时，来到该处处理
				//认证失败，请重新认证                    
                    break;                    
                case 'denied':
				//auth的stage为login时，被服务器返回AUTH_DENIED时，来到该处处理
				//认证被拒
                    break;                    
                case 'activate': 
				//auth的stage为login时，被服务器返回AUTH_VALIDATION时，来到该处处理
				//待激活
                    break;
                default:
                    break;
            }
        }else{
            //不回显任何信息
        }
    }

    /**
     * 获取验证码图片地址
     * @return string 验证码图片
     */
    public function verifyImg(){
        $verify = new \Think\Verify;
        $verify->fontSize = 18;
        $verify->imageH = 40;
        $verify->imageW = 150;
        $verify->length = 4;
        //$Verify->fontttf = '4.ttf'; 
        $verify->entry();
    }

    /**
     * 注册表单异步处理页面
     * @return [type] [description]
     */
    public function signUpFormHandle(){
    	if(!IS_AJAX) E('页面不存在！');
    	$username = I('username');
      	$password = I('password');
      	$password_re = I('password_re');
      	$email = I('email');
      	$verifycode = I('verifycode',0);
      	$result = array();
      	$user = array();

      	//检查验证码
      	$verify = new \Think\Verify();
        $verifySessionID = $verifySessionID = verifyauthcode($verify->seKey);
        $rightVerifyCode = $_SESSION[$verifySessionID]['verify_code']; //正确的加密后的验证码
      	if($verifycode!='' && verifyauthcode(strtoupper($verifycode))==$rightVerifyCode){
      		//验证成功
      	}
      	else{
      		//验证失败
      		$this->ajaxReturn(array('status'=>0,'obj'=>'verifycode','msg'=>'验证码错误！'));
      	}

      	//检查用户名
      	if($username!=''){
      		$usernamePreg = '/^[a-zA-Z_]\w{3,20}[a-zA-Z0-9]$/is';
      		if(preg_match($usernamePreg, $username)){
      			//验证用户名是否存在
      			if( M('user')->where(array('username'=>$username))->find() ){
      				$this->ajaxReturn(array('status'=>0,'obj'=>'username','msg'=>'用户名已经存在！'));
      			}
      			else{
      				$user['username'] = $username;
      			}
      		}
      		else{
      			//验证失败
      			$this->ajaxReturn(array('status'=>0,'obj'=>'username','msg'=>'用户名必须是3-20个字符，以英文或下划线开头，可以包含英文、数字或下划线。'));
      		}
      	}
      	else{
      		//验证失败
      		$this->ajaxReturn(array('status'=>0,'obj'=>'username','msg'=>'用户名不能为空！'));
      	}

      	//检查密码
      	if($password!=''){
      		if(strlen($password)<8){
      				$this->ajaxReturn(array('status'=>0,'obj'=>'password','msg'=>'密码长度必须8位以上！'));
      		}
      		if($password_re!=''){
	      		if($password==$password_re){
	      			//验证成功
	      			$user['password'] = md5($password);
	      		}
	      		else{
	      			$this->ajaxReturn(array('status'=>0,'obj'=>'password_re','msg'=>'两次输入的密码不相同！'));
	      		}
      		}
      		else{
      			$this->ajaxReturn(array('status'=>0,'obj'=>'password_re','msg'=>'请再次输入密码！'));
      		}
      	}
      	else{
      		//验证失败
      		$this->ajaxReturn(array('status'=>0,'obj'=>'password','msg'=>'密码不能为空！'));
      	}

      	//检查E-mail
      	if($email!=''){
      		$emailPreg = '/^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/';
      		if(preg_match($emailPreg, $email)){
      			//检查Email地址是否重复
      			if( M('user')->where(array('email'=>$email))->find() ){
      				$this->ajaxReturn(array('status'=>0,'obj'=>'email','msg'=>'Email地址已经存在！'));
      			}
      			else{
      				//验证成功
      				$user['email'] = $email;
      			}
      		}
      		else{
      			//验证失败
      			$this->ajaxReturn(array('status'=>0,'obj'=>'email','msg'=>'Email地址格式不正确！'));
      		}
      	}
      	else{
      		//验证失败
      		$this->ajaxReturn(array('status'=>0,'obj'=>'email','msg'=>'Email地址不能为空！'));
      	}
      	date_default_timezone_set('Asia/Shanghai'); 
      	$user['registertime'] = time();
      	$user['logintime'] = time();
      	$user['ctype'] = 0;
      	$user['islock'] = 0;
      	//p($user);
      	//插入数据库
      	$mid = M('user')->add($user);
      	if($mid){
      		//写入session
    		//session('uid', $mid);
    		//session('username', $user['username']);
    		//session('logintime', $user['logintime']);
    		//session('email', $user['email']);
      		recordLog('用户' . $user['username'] . '在' . date('Y-m-d H:i:s', $user['registertime']) . '注册成功。', time());
      		$this->ajaxReturn(array('status'=>1,'obj'=>'main','msg'=>'注册成功！'));
      	}
      	else{
      		$this->ajaxReturn(array('status'=>0,'obj'=>'main','msg'=>'未知错误，刷新重试！'));
      	}
    }
}