<?php
//数组打印函数
function p($arr){
    dump($arr,1,'',0);
}

//日志记录函数
function recordLog($msg, $time){
	M('log')->add(array('msg' => $msg, 'time' => $time));
}

// 检测输入的验证码是否正确，$code为用户输入的验证码字符串
function check_verify($code, $id = ''){
    $verify = new \Think\Verify();
    return $verify->check($code, $id);
}

// ThinkPHP 加密验证码算法，这里重写，以便验证
function verifyauthcode($str){
	$verify = new \Think\Verify();
    $key = substr(md5($verify->seKey), 5, 8);
    $str = substr(md5($str), 8, 10);
    return md5($key . $str);
}

/**
 * 秒数格式化函数，用于将特定秒数格式化成为小时分钟秒的格式
 * @param  [type] $second [description]
 * @return [type]         [description]
 */
function secFormat($second){
	$hour = floor($second/3600);
	$second = $second - $hour * 3600;
	$min = floor($second / 60);
	$second = $second - $min * 60;
	$sec = $second;
	return (($hour>0)?$hour.':':'').(($min>0)?$min.':':'').$sec;
}

/**
 * 流量数据格式化，由B自动转为KB或MB
 * @param  [type] $flow [description]
 * @return [type]       [description]
 */
function flowFormat($flow){
	if(floor($flow/1024)==0){
		return $flow.'B';
	}
	else if(floor($flow/1024)<1024){
		return sprintf("%.2f", $flow/1024).'KB';
	}
	else{
		return sprintf("%.2f", $flow/1024/1024).'MB';
	}

}
?>