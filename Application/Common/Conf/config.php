<?php
return array(
	//默认模块
    'DEFAULT_MODULE' => 'Home',
    //点语法默认解析
    'TMPL_VAR_IDENTIFY' => 'array',
    //模板文件名连接符
    'TMPL_FILE_DEPR' => '_',
    //数据库相关配置项
    'DB_TYPE'               =>  'mysql',          // 数据库类型
    'DB_HOST'               =>  'localhost',      // 服务器地址
    'DB_NAME'               =>  'wifidog', // 数据库名
    'DB_USER'               =>  'root',           // 用户名
    'DB_PWD'                =>  '',               // 密码
    'DB_PORT'               =>  '3306',           // 端口
    'DB_PREFIX'             =>  'wd_',           // 数据库表前缀
    //自定义SESSION数据库存储
    'SESSION_TYPE' => 'Db',
);