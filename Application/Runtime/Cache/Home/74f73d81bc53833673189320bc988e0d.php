<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="/wifidog/Public/Home/main/images/favicon.png" type="image/png">
    <title>控制面板</title>
    <link href="/wifidog/Public/Home/main/css/style.default.css" rel="stylesheet">
    <link href="/wifidog/Public/Home/main/css/jquery.datatables.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
  <script src="/wifidog/Public/Home/main/js/html5shiv.js"></script>
  <script src="/wifidog/Public/Home/main/js/respond.min.js"></script>
  <![endif]-->
</head>
<body>
<section>

  <div style="display:;margin-left:10%;margin-right:10%;margin-top:8%;background: none repeat scroll 0 0 #e4e7ea;">
    
    <div class="pageheader">
      <h2><i class="fa fa-home"></i> 控制面板 <span>更多功能，敬请期待！</span></h2>
      <div style="display:none;" class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a >Bracket</a></li>
          <li class="active">Dashboard</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
      
      <div class="row">
        
        <div class="col-sm-0 col-md-3">
        </div><!-- col-sm-6 -->
        
        <div class="col-sm-6 col-md-3">
          <div class="panel panel-primary panel-stat">
            <div class="panel-heading">
              
              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <img src="/wifidog/Public/Home/main/images/is-user.png" alt="" />
                  </div>
                  <div class="col-xs-8">
                    <small class="stat-label">你好！</small>
                    <h1><?php echo ($username); ?></h1>
                  </div>
                </div><!-- row -->
                
                <div class="mb15"></div>
                
                <div class="row">
                  <div class="col-xs-12">
                    <small class="stat-label">上次登录时间：</small>
                    <h4><?php echo ($last_login_time); ?></h4>
                  </div>
                </div>  
                <div class="row">
                  <div class="col-xs-12">
                    <small class="stat-label">上次使用流量：</small>
                    <h4><?php echo ($last_online_flow); ?></h4>
                  </div>
                </div><!-- row -->
              </div><!-- stat -->
              
            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->
        
        <div class="col-sm-6 col-md-3">
          <div class="panel panel-<?php if($is_online == 0): ?>danger<?php else: ?>success<?php endif; ?> panel-stat">
            <div class="panel-heading">
              
              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <img src="/wifidog/Public/Home/main/images/is-document.png" alt="" />
                  </div>
                  <div class="col-xs-8">
                    <small class="stat-label">剩余流量：</small>
                    <h1><?php echo ($remainflow); ?></h1>
                  </div>
                </div><!-- row -->
                
                <div class="mb15"></div>
                
                <div class="row">
                  <div class="col-xs-6">
                    <small class="stat-label">本次上线计时：</small>
                    <h4>12:12:12</h4>
                  </div>
                  <div class="col-xs-6">
                    <small class="stat-label">上次上线计时：</small>
                    <h4><?php echo ($last_online_time); ?></h4>
                  </div>
                </div>  
                <div class="mb15"></div>
                <div class="row">
                  <div class="col-xs-12">
                  <h4>当前状态：<?php if($is_online == 0): ?>离线<?php else: ?>在线<?php endif; ?></h4>
                  </div>
                </div><!-- row -->
                  
              </div><!-- stat -->
              
            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->
        
        <div class="col-sm-0 col-md-3">
        </div><!-- col-sm-6 -->
      </div><!-- row -->
      
      <div class="row">
        <div class="col-sm-2 col-md-4"></div>
        <div class="col-sm-8 col-md-4">
            <a class="btn btn-warning">&nbsp;&nbsp;&nbsp;充值&nbsp;&nbsp;&nbsp;</a>&nbsp;&nbsp;&nbsp;
            <a class="btn btn-success" target="_blank" href="<?php echo U('/Home/Index/on_line');?>">&nbsp;&nbsp;&nbsp;上线&nbsp;&nbsp;&nbsp;</a>&nbsp;&nbsp;&nbsp;
            <a class="btn btn-danger" href="<?php echo U('/Home/Index/off_line');?>">&nbsp;&nbsp;&nbsp;下线&nbsp;&nbsp;&nbsp;</a>&nbsp;&nbsp;&nbsp;
            <a class="btn btn-info" href="<?php echo U('/Home/Index/logout');?>">退出登录</a>
        </div>
        <div class="col-sm-2 col-md-4"></div>
      </div>
      <div class="mb15"></div>
      <div class="row">
        <div class="col-sm-2 col-md-4"></div>
        <div class="col-sm-8 col-md-4">
            注意：请勿关闭本页面，否则会自动下线，退出登录后会自动下线。
        </div>
        <div class="col-sm-2 col-md-4"></div>
      </div>

      <div style="display:none;" class="row">
        <div class="col-sm-8 col-md-9">
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="row">
                <div class="col-sm-8">
                  <h5 class="subtitle mb5">Network Performance</h5>
                  <p class="mb15">Duis autem vel eum iriure dolor in hendrerit in vulputate...</p>
                  <div id="basicflot" style="width: 100%; height: 300px; margin-bottom: 20px"></div>
                </div><!-- col-sm-8 -->
                <div class="col-sm-4">
                  <h5 class="subtitle mb5">Server Status</h5>
                  <p class="mb15">Summary of the status of your server.</p>
                </div><!-- col-sm-4 -->
              </div><!-- row -->
            </div><!-- panel-body -->
          </div><!-- panel -->
        </div><!-- col-sm-9 -->
        
        <div class="col-sm-4 col-md-3">
          
          <div class="panel panel-default">
            <div class="panel-body">
            <h5 class="subtitle mb5">Most Browser Used</h5>
            <p class="mb15">Duis autem vel eum iriure dolor in hendrerit in vulputate...</p>
            <div id="donut-chart2" style="text-align: center; height: 298px;"></div>
            </div><!-- panel-body -->
          </div><!-- panel -->
          
        </div><!-- col-sm-3 -->
        
      </div><!-- row -->
      
    </div><!-- contentpanel -->
    
  </div><!-- mainpanel -->
  
</section>

<script src="/wifidog/Public/Home/main/js/jquery-1.10.2.min.js"></script>
<script>
  var CONTROL = '<?php echo U("/Home/Index/",'','');?>';  //异步验证基地址
  var int=self.setInterval("update()", 1000);
  var startTime = <?php echo ($start_online_time); ?>;
  var cnt = 0;
  var onlineTime = 0;
  update();
  function update(){
    if((cnt++)%5==0){
      $.ajax({
        url: CONTROL+"/onlineDataQuery",
        type:"POST",
        data:{'time':Date.parse(new Date())},
        timeout:10000,
        dataType:"json",
        success:function(data){
           if(data.status==1){
            $('h1:eq(1)').text(flowFormat(data.remainflow));
            if(data.is_online){
              $('h4:eq(4)').text('当前状态：在线');
              $('.panel:eq(1)').removeClass('panel-danger');
              $('.panel:eq(1)').addClass('panel-success');
            }
            else{
              $('h4:eq(4)').text('当前状态：离线');
              $('.panel:eq(1)').removeClass('panel-success');
              $('.panel:eq(1)').addClass('panel-danger');
            }
           }
        },
        error:function(){
        }
     });
    }
    if(startTime>0){
      onlineTime = Date.parse(new Date())/1000 - startTime;
    }
    else{
      onlineTime = 0;
    }
    $('h4:eq(2)').text(secFormat(onlineTime));
  }

  /**
   * 秒数格式化函数，用于将特定秒数格式化成为小时分钟秒的格式
   * @param  [type] $second [description]
   * @return [type]         [description]
   */
  function secFormat(second){
    var hour = Math.floor(second/3600);
    second = second - hour * 3600;
    var min = Math.floor(second / 60);
    second = second - min * 60;
    var sec = second;
    return ((hour>0)?hour+':':'')+((min>0)?min+':':'')+sec;
  }

  /**
   * 流量数据格式化，由B自动转为KB或MB
   * @param  [type] $flow [description]
   * @return [type]       [description]
   */
  function flowFormat(flow){
    if(Math.floor(flow/1024)==0){
      return flow+'B';
    }
    else if(Math.floor(flow/1024)<1024){
      return (flow/1024).toFixed(2)+'KB';
    }
    else{
      return (flow/1024/1024).toFixed(2)+'MB';
    }

  }
</script>

</body>
</html>