
jQuery(window).load(function() {
   
   // Page Preloader
   jQuery('#status').fadeOut();
   jQuery('#preloader').delay(100).fadeOut(function(){
      jQuery('body').delay(100).css({'overflow':'visible'});
   });
});

jQuery(document).ready(function() {
   
   // Toggle Left Menu
   jQuery('.leftpanel .nav-parent > a').live('click', function() {
      
      var parent = jQuery(this).parent();
      var sub = parent.find('> ul');
      
      // Dropdown works only when leftpanel is not collapsed
      if(!jQuery('body').hasClass('leftpanel-collapsed')) {
         if(sub.is(':visible')) {
            sub.slideUp(200, function(){
               parent.removeClass('nav-active');
               jQuery('.mainpanel').css({height: ''});
               adjustmainpanelheight();
            });
         } else {
            closeVisibleSubMenu();
            parent.addClass('nav-active');
            sub.slideDown(200, function(){
               adjustmainpanelheight();
            });
         }
      }
      return false;
   });
   
   function closeVisibleSubMenu() {
      jQuery('.leftpanel .nav-parent').each(function() {
         var t = jQuery(this);
         if(t.hasClass('nav-active')) {
            t.find('> ul').slideUp(200, function(){
               t.removeClass('nav-active');
            });
         }
      });
   }
   
   function adjustmainpanelheight() {
      // Adjust mainpanel height
      var docHeight = jQuery(document).height();
      if(docHeight > jQuery('.mainpanel').height())
         jQuery('.mainpanel').height(docHeight);
   }
   adjustmainpanelheight();
   
   
   // Tooltip
   jQuery('.tooltips').tooltip({ container: 'body'});
   
   // Popover
   jQuery('.popovers').popover();
   
   // Close Button in Panels
   jQuery('.panel .panel-close').click(function(){
      jQuery(this).closest('.panel').fadeOut(200);
      return false;
   });
   
   // Form Toggles
   jQuery('.toggle').toggles({on: true});
   
   jQuery('.toggle-chat1').toggles({on: false});
   
   // Sparkline
   jQuery('#sidebar-chart').sparkline([4,3,3,1,4,3,2,2,3,10,9,6], {
	  type: 'bar', 
	  height:'30px',
      barColor: '#428BCA'
   });
   
   jQuery('#sidebar-chart2').sparkline([1,3,4,5,4,10,8,5,7,6,9,3], {
	  type: 'bar', 
	  height:'30px',
      barColor: '#D9534F'
   });
   
   jQuery('#sidebar-chart3').sparkline([5,9,3,8,4,10,8,5,7,6,9,3], {
	  type: 'bar', 
	  height:'30px',
      barColor: '#1CAF9A'
   });
   
   jQuery('#sidebar-chart4').sparkline([4,3,3,1,4,3,2,2,3,10,9,6], {
	  type: 'bar', 
	  height:'30px',
      barColor: '#428BCA'
   });
   
   jQuery('#sidebar-chart5').sparkline([1,3,4,5,4,10,8,5,7,6,9,3], {
	  type: 'bar', 
	  height:'30px',
      barColor: '#F0AD4E'
   });
   
   
   // Minimize Button in Panels
   jQuery('.minimize').click(function(){
      var t = jQuery(this);
      var p = t.closest('.panel');
      if(!jQuery(this).hasClass('maximize')) {
         p.find('.panel-body, .panel-footer').slideUp(200);
         t.addClass('maximize');
         t.html('&plus;');
      } else {
         p.find('.panel-body, .panel-footer').slideDown(200);
         t.removeClass('maximize');
         t.html('&minus;');
      }
      return false;
   });
   
   
   // Add class everytime a mouse pointer hover over it
   jQuery('.nav-bracket > li').hover(function(){
      jQuery(this).addClass('nav-hover');
   }, function(){
      jQuery(this).removeClass('nav-hover');
   });
   
   
   // Menu Toggle
   jQuery('.menutoggle').click(function(){
      
      var body = jQuery('body');
      var bodypos = body.css('position');
      
      if(bodypos != 'relative') {
         
         if(!body.hasClass('leftpanel-collapsed')) {
            body.addClass('leftpanel-collapsed');
            jQuery('.nav-bracket ul').attr('style','');
            
            jQuery(this).addClass('menu-collapsed');
            
         } else {
            body.removeClass('leftpanel-collapsed chat-view');
            jQuery('.nav-bracket li.active ul').css({display: 'block'});
            
            jQuery(this).removeClass('menu-collapsed');
            
         }
      } else {
         
         if(body.hasClass('leftpanel-show'))
            body.removeClass('leftpanel-show');
         else
            body.addClass('leftpanel-show');
         
         adjustmainpanelheight();         
      }

   });
   
   // Chat View
   jQuery('#chatview').click(function(){
      
      var body = jQuery('body');
      var bodypos = body.css('position');
      
      if(bodypos != 'relative') {
         
         if(!body.hasClass('chat-view')) {
            body.addClass('leftpanel-collapsed chat-view');
            jQuery('.nav-bracket ul').attr('style','');
            
         } else {
            
            body.removeClass('chat-view');
            
            if(!jQuery('.menutoggle').hasClass('menu-collapsed')) {
               jQuery('body').removeClass('leftpanel-collapsed');
               jQuery('.nav-bracket li.active ul').css({display: 'block'});
            } else {
               
            }
         }
         
      } else {
         
         if(!body.hasClass('chat-relative-view')) {
            
            body.addClass('chat-relative-view');
            body.css({left: ''});
         
         } else {
            body.removeClass('chat-relative-view');   
         }
      }
      
   });
   
   reposition_topnav();
   reposition_searchform();
   
   jQuery(window).resize(function(){
      
      if(jQuery('body').css('position') == 'relative') {

         jQuery('body').removeClass('leftpanel-collapsed chat-view');
         
      } else {
         
         jQuery('body').removeClass('chat-relative-view');         
         jQuery('body').css({left: '', marginRight: ''});
      }
      
      reposition_searchform();
      reposition_topnav();
      
   });
   
   
   
   /* This function will reposition search form to the left panel when viewed
    * in screens smaller than 767px and will return to top when viewed higher
    * than 767px
    */ 
   function reposition_searchform() {
      if(jQuery('.searchform').css('position') == 'relative') {
         jQuery('.searchform').insertBefore('.leftpanelinner .userlogged');
      } else {
         jQuery('.searchform').insertBefore('.header-right');
      }
   }
   
   

   /* This function allows top navigation menu to move to left navigation menu
    * when viewed in screens lower than 1024px and will move it back when viewed
    * higher than 1024px
    */
   function reposition_topnav() {
      if(jQuery('.nav-horizontal').length > 0) {
         
         // top navigation move to left nav
         // .nav-horizontal will set position to relative when viewed in screen below 1024
         if(jQuery('.nav-horizontal').css('position') == 'relative') {
                                  
            if(jQuery('.leftpanel .nav-bracket').length == 2) {
               jQuery('.nav-horizontal').insertAfter('.nav-bracket:eq(1)');
            } else {
               // only add to bottom if .nav-horizontal is not yet in the left panel
               if(jQuery('.leftpanel .nav-horizontal').length == 0)
                  jQuery('.nav-horizontal').appendTo('.leftpanelinner');
            }
            
            jQuery('.nav-horizontal').css({display: 'block'})
                                  .addClass('nav-pills nav-stacked nav-bracket');
            
            jQuery('.nav-horizontal .children').removeClass('dropdown-menu');
            jQuery('.nav-horizontal > li').each(function() { 
               
               jQuery(this).removeClass('open');
               jQuery(this).find('a').removeAttr('class');
               jQuery(this).find('a').removeAttr('data-toggle');
               
            });
            
            if(jQuery('.nav-horizontal li:last-child').has('form')) {
               jQuery('.nav-horizontal li:last-child form').addClass('searchform').appendTo('.topnav');
               jQuery('.nav-horizontal li:last-child').hide();
            }
         
         } else {
            // move nav only when .nav-horizontal is currently from leftpanel
            // that is viewed from screen size above 1024
            if(jQuery('.leftpanel .nav-horizontal').length > 0) {
               
               jQuery('.nav-horizontal').removeClass('nav-pills nav-stacked nav-bracket')
                                        .appendTo('.topnav');
               jQuery('.nav-horizontal .children').addClass('dropdown-menu').removeAttr('style');
               jQuery('.nav-horizontal li:last-child').show();
               jQuery('.searchform').removeClass('searchform').appendTo('.nav-horizontal li:last-child .dropdown-menu');
               jQuery('.nav-horizontal > li > a').each(function() {
                  
                  jQuery(this).parent().removeClass('nav-active');
                  
                  if(jQuery(this).parent().find('.dropdown-menu').length > 0) {
                     jQuery(this).attr('class','dropdown-toggle');
                     jQuery(this).attr('data-toggle','dropdown');  
                  }
                  
               });              
            }
            
         }
         
      }
   }
   
   
   // Sticky Header
   if(jQuery.cookie('sticky-header'))
      jQuery('body').addClass('stickyheader');
      
   // Sticky Left Panel
   if(jQuery.cookie('sticky-leftpanel')) {
      jQuery('body').addClass('stickyheader');
      jQuery('.leftpanel').addClass('sticky-leftpanel');
   }
   
   // Left Panel Collapsed
   if(jQuery.cookie('leftpanel-collapsed')) {
      jQuery('body').addClass('leftpanel-collapsed');
      jQuery('.menutoggle').addClass('menu-collapsed');
   }
   
   // Changing Skin
   var c = jQuery.cookie('change-skin');
   if(c) {
      jQuery('head').append('<link id="skinswitch" rel="stylesheet" href="css/style.'+c+'.css" />');
   }
   
   // Changing Font
   var fnt = jQuery.cookie('change-font');
   if(fnt) {
      jQuery('head').append('<link id="fontswitch" rel="stylesheet" href="css/font.'+fnt+'.css" />');
   }
   
   // Check if leftpanel is collapsed
   if(jQuery('body').hasClass('leftpanel-collapsed'))
      jQuery('.nav-bracket .children').css({display: ''});
      
     
   // Handles form inside of dropdown 
   jQuery('.dropdown-menu').find('form').click(function (e) {
      e.stopPropagation();
    });
      

});

/* =======================自定义JS代码====================== */

   //改变验证码URL地址
   function change_code(obj){
      $("#verifyImg").attr("src",verifyImgURL + "/" + Math.random());
      return false;
   }
   /**
     * 显示输入框上面的错误信息
     * @param  {[obj]} Obj     [显示错误的对象]
     * @param  {[string]} Msg  [错误信息]
     * @param  {[int]} error=1 [是否显示错误信息，error=1显示错误，error=0清除错误]
     * @return {[type]}        [description]
     */
   function displayAlert(Obj, Msg, error){
      var error = typeof error == 'undefined' ? 1 : error;
      if(error){
         Obj.parents('.mb10:eq(0)').addClass('has-error');  //输入框变红
         Obj.prev('label').css('display','');  //显示错误信息
         Obj.prev('label').children('span:eq(0)').text(Msg);
      }
      else{
         Obj.parents('.mb10:eq(0)').removeClass('has-error');  //恢复输入框
         Obj.prev('label').css('display','none');  //隐藏错误信息
         Obj.prev('label').children('span:eq(0)').text('');
      }
   }

   $("#register_form").submit(function(e){
      e.preventDefault();
      var usernameObj = $('#username');
      var passwordObj = $('#password');
      var password_reObj = $('#password_re');
      var emailObj = $('#email');
      var verifyCodeObj = $('#verifycode');

      //验证用户名
      if(usernameObj.val().trim()==''){
         displayAlert(usernameObj, '用户名不能为空！');
         return false;
      }
      else{
         var usernamePreg = /^[a-zA-Z_]\w{3,20}[a-zA-Z0-9]$/ig;
         if(!usernamePreg.test(usernameObj.val().trim())){
            displayAlert(usernameObj, '用户名必须是5-20个字符，以英文或下划线开头，可以包含英文、数字或下划线。');
            return false;
         }
         else{
            displayAlert(usernameObj, '', 0);
         }
      }

      //验证密码
      if(passwordObj.val().trim()==''){
         displayAlert(passwordObj, '密码不能为空！');
         return false;
      }
      else{
         if(passwordObj.val().trim().length>=8){
            displayAlert(passwordObj, '', 0);
            if(password_reObj.val().trim()==''){
               displayAlert(password_reObj, '请再次输入密码！');
               return false;
            }
            else{
               if(passwordObj.val().trim()!=password_reObj.val().trim()){
                  displayAlert(password_reObj, '两次密码不相同！');
                  return false;
               }
               else{
                  displayAlert(password_reObj, '', 0);
               }
            }
         }
         else{
            displayAlert(passwordObj, '密码长度必须大于8位！');
            return false;
         }
      }

      //验证邮箱      
      if(emailObj.val().trim()==''){
         displayAlert(emailObj, 'Email地址不能为空！');
         return false;
      }
      else{
         displayAlert(emailObj, '', 0);
         var emailPreg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
         if(!emailPreg.test(emailObj.val().trim()))
         {
            displayAlert(emailObj, 'Email地址格式不正确！');
            emailObj.focus();
            return false;
         }
      }

      //验证验证码
      if(verifyCodeObj.val().trim()==''){
         verifyCodeObj.parents('.mb10:eq(0)').addClass('has-error');  //输入框变红
         verifyCodeObj.parents('.row').prev('label').css('display','');  //显示错误信息
         verifyCodeObj.parents('.row').prev('label').children('span:eq(0)').text('验证码不能为空！');
         return false;
      }
      else{
         verifyCodeObj.parents('.mb10:eq(0)').removeClass('has-error');  //输入框变红
         verifyCodeObj.parents('.row').prev('label').css('display','none');  //显示错误信息
         verifyCodeObj.parents('.row').prev('label').children('span:eq(0)').text('');
      }

      //ajax查询email,用户名是否重复,验证码是否正确
      $.ajax({
         url: CONTROL+"/signUpFormHandle",
         type:"POST",
         data:{username:usernameObj.val().trim(),password:passwordObj.val().trim(),password_re:password_reObj.val().trim(),email:emailObj.val().trim(),verifycode:verifyCodeObj.val().trim()},
         timeout:10000,
         dataType:"json",
         success:function(data){
            $('#main_alert').css('display','none');
            if(data.status==1){
               window.location.href = redirectUrl; 
            }
            else{
               switch(data.obj){
                  case 'main':
                     $('#main_alert').css('display','');
                     $('#main_alert').children('span:eq(0)').text(data.msg);
                     break;
                  case 'username':
                     displayAlert(usernameObj, data.msg);
                     break;
                  case 'password':
                     displayAlert(passwordObj, data.msg);
                     break;
                  case 'password_re':
                     displayAlert(password_reObj, data.msg);
                     break;
                  case 'email':
                     displayAlert(emailObj, data.msg);
                     break;
                  case 'verifycode':
                     verifyCodeObj.parents('.mb10:eq(0)').addClass('has-error');  //输入框变红
                     verifyCodeObj.parents('.row').prev('label').css('display','');  //显示错误信息
                     verifyCodeObj.parents('.row').prev('label').children('span:eq(0)').text(data.msg);
                     break;
                  default:break;
               }
            }
         },
         error:function(){
            $('#main_alert').css('display','');
            $('#main_alert').children('span:eq(0)').text('未知错误，请稍后重试！');
         }
     });
      return false;
   });


   $("#login_form").submit(function(e){
         e.preventDefault();
         var usernameObj = $('#login_username');
         var passwordObj = $('#login_password');
         //var verifyCodeObj = $('#verifycode');

         //验证用户名
         if(usernameObj.val().trim()==''){
            $('#login_error_msg').css('display','');
            $('#login_error_msg').children('span:eq(0)').text('用户名不能为空！');
            return false;
         }
         else{
            //验证密码
            if(passwordObj.val().trim()==''){
               $('#login_error_msg').css('display','');
               $('#login_error_msg').children('span:eq(0)').text('密码不能为空！');
               return false;
            }
            else{
               //ajax查询email,用户名是否重复,验证码是否正确
               $.ajax({
                  url: CONTROL+"/loginFormHandle",
                  type:"POST",
                  data:{username:usernameObj.val().trim(),password:passwordObj.val().trim()},
                  timeout:10000,
                  dataType:"json",
                  success:function(data){
                     if(data.status==1){
                        window.location.href = CONTROL + "/dashboard"; 
                     }
                     else{
                        $('#login_error_msg').css('display','');
                        $('#login_error_msg').children('span:eq(0)').text(data.Msg);
                     }
                  },
                  error:function(){
                     $('#login_error_msg').css('display','');
                     $('#login_error_msg').children('span:eq(0)').text('未知错误，请稍后重试！');
                  }
               });
            }
         }
         return false;
      });
/* ========================================================= */