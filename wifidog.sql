-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2014-07-22 05:40:15
-- 服务器版本： 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wifidog`
--
CREATE DATABASE IF NOT EXISTS `wifidog` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `wifidog`;

-- --------------------------------------------------------

--
-- 表的结构 `wd_log`
--

CREATE TABLE IF NOT EXISTS `wd_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg` varchar(100) COLLATE utf8_bin NOT NULL,
  `time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=29 ;

--
-- 转存表中的数据 `wd_log`
--

INSERT INTO `wd_log` (`id`, `msg`, `time`) VALUES
(1, '', 0),
(5, '2014', 1405862239),
(4, '2014', 1405861195),
(6, '2014', 1405865764),
(7, '2014', 1405865996),
(8, '2014', 1405914332),
(9, '2014', 1405914551),
(10, '2014', 1405914635),
(11, '网关ID:,设备:,通过 : 请求认证。', 1405936938),
(12, '登录失败！', 1405936946),
(13, '登录失败！', 1405936952),
(14, 'zsjinwei登录成功！', 1405936959),
(15, '网关ID:,设备:,通过 : 请求认证。', 1405942820),
(16, 'zsjinwei登录成功！', 1405942827),
(17, '网关ID:,设备:,通过 : 请求认证。', 1405943787),
(18, 'zsjinwei登录成功！', 1405943794),
(19, '网关ID:,设备:,通过 : 请求认证。', 1405945103),
(20, 'zsjinwei登录成功！', 1405945110),
(21, '网关ID:,设备:,通过 : 请求认证。', 1405994699),
(22, 'zsjinwei登录成功！', 1405994707),
(23, '网关ID:,设备:,通过 : 请求认证。', 1405996356),
(24, 'zsjinwei登录成功！', 1405996363),
(25, 'zsjinwei登录成功！', 1405998913),
(26, 'zsjinwei登录成功！', 1405999080),
(27, '网关ID:,设备:,通过 : 请求认证。', 1406000136),
(28, 'zsjinwei登录成功！', 1406000143);

-- --------------------------------------------------------

--
-- 表的结构 `wd_session`
--

CREATE TABLE IF NOT EXISTS `wd_session` (
  `session_id` varchar(255) NOT NULL,
  `session_expire` int(11) NOT NULL,
  `session_data` blob,
  UNIQUE KEY `session_id` (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `wd_session`
--

INSERT INTO `wd_session` (`session_id`, `session_expire`, `session_data`) VALUES
('4ck8o5fck2365m4apkbboni5s0', 1406001421, ''),
('msl30rk2ich5ui7blfemmn27q5', 1406001851, 0x7569647c733a313a2231223b757365726e616d657c733a383a227a736a696e776569223b6c6f67696e74696d657c733a31303a2231343035393939303830223b656d61696c7c733a31373a227a736a696e776569406c6976652e636f6d223b746f6b656e7c733a33323a223332666266386633613233393336623538616534653764326363613930666662223b72656d61696e666c6f777c733a363a22313233343536223b73746172745f6f6e6c696e655f74696d657c693a303b);

-- --------------------------------------------------------

--
-- 表的结构 `wd_user`
--

CREATE TABLE IF NOT EXISTS `wd_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` char(20) NOT NULL DEFAULT '',
  `password` char(32) NOT NULL DEFAULT '',
  `registertime` int(10) unsigned NOT NULL,
  `logintime` int(10) unsigned NOT NULL,
  `email` varchar(30) NOT NULL,
  `ctype` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `remaintime` int(11) NOT NULL DEFAULT '0',
  `remainflow` float NOT NULL DEFAULT '0',
  `islock` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- 转存表中的数据 `wd_user`
--

INSERT INTO `wd_user` (`id`, `username`, `password`, `registertime`, `logintime`, `email`, `ctype`, `remaintime`, `remainflow`, `islock`) VALUES
(1, 'zsjinwei', 'd0e46ad934a0d81cb9ba2e7c7c453cf0', 0, 1406000143, 'zsjinwei@live.com', 0, 0, 123456, 0),
(3, 'testuser', '5d9c68c6c50ed3d02a2fcf54f63993b6', 1405861195, 1405861195, 'test@test.com', 0, 0, 10, 0);

-- --------------------------------------------------------

--
-- 表的结构 `wd_user_last_online_data`
--

CREATE TABLE IF NOT EXISTS `wd_user_last_online_data` (
  `username` varchar(30) NOT NULL,
  `last_online_time` int(10) unsigned NOT NULL,
  `last_online_flow` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `wd_user_last_online_data`
--

INSERT INTO `wd_user_last_online_data` (`username`, `last_online_time`, `last_online_flow`) VALUES
('zsjinwei', 1234564, 1234);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
